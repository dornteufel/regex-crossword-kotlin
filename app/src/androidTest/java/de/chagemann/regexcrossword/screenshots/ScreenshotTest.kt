package de.chagemann.regexcrossword.screenshots

import android.Manifest
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.GrantPermissionRule
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.features.selectcategory.CategoryAdapter
import de.chagemann.regexcrossword.features.selectcategory.SelectCategoryActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy
import tools.fastlane.screengrab.locale.LocaleTestRule

@RunWith(AndroidJUnit4::class)
class ScreenshotTest {

    @Rule
    @JvmField
    val localeTestRule = LocaleTestRule()

    @Rule
    @JvmField
    val permissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        Manifest.permission.CHANGE_CONFIGURATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    @Rule
    @JvmField
    val activityRule = ActivityScenarioRule(SelectCategoryActivity::class.java)

    @Test
    fun testTakeScreenshot() {
        Screengrab.setDefaultScreenshotStrategy(UiAutomatorScreenshotStrategy())

        Screengrab.screenshot("1-select-category")

        onView(
            withId(R.id.categoryRecyclerView)
        ).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryAdapter.ViewHolder>(0, click()))

        Screengrab.screenshot("2-select-level")

        onView(
            withId(R.id.levelRecyclerView)
        ).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryAdapter.ViewHolder>(0, click()))
        closeSoftKeyboard()

        Screengrab.screenshot("3-tutorial-level")

        pressBack()
        pressBack()

        onView(
            withId(R.id.categoryRecyclerView)
        ).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryAdapter.ViewHolder>(2, click()))

        onView(
            withId(R.id.levelRecyclerView)
        ).perform(RecyclerViewActions.actionOnItemAtPosition<CategoryAdapter.ViewHolder>(0, click()))

        closeSoftKeyboard()
        Screengrab.screenshot("4-tutorial-experienced")
    }
}
