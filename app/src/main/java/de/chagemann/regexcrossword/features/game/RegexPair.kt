package de.chagemann.regexcrossword.features.game

import java.util.regex.Pattern

data class RegexPair(val regexRule: String, val stringToMatch: String)

fun RegexPair.isValid(): Boolean {
    return Pattern.matches(regexRule, stringToMatch)
}