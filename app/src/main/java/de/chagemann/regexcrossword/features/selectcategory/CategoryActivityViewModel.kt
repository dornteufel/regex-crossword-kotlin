package de.chagemann.regexcrossword.features.selectcategory

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import de.chagemann.regexcrossword.db.CrosswordCategory
import de.chagemann.regexcrossword.db.CrosswordRepository

class CategoryActivityViewModel(private val app: Application) : AndroidViewModel(app) {

    private val repository: CrosswordRepository by lazy {
        CrosswordRepository(app)
    }

    val crosswordCategoryList: LiveData<List<CrosswordCategory>> by lazy {
        loadCategoryList()
    }

    private fun loadCategoryList(): LiveData<List<CrosswordCategory>> {
        return repository.getCrosswordCategories()
    }
}