package de.chagemann.regexcrossword.features.selectlevel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import de.chagemann.regexcrossword.db.Crossword
import de.chagemann.regexcrossword.db.CrosswordRepository
import de.chagemann.regexcrossword.db.LevelCategory

class SelectLevelActivityViewModel(private val app: Application, val levelCategory: LevelCategory) : AndroidViewModel(app) {

    private val repository: CrosswordRepository by lazy {
        CrosswordRepository(app)
    }

    val crosswordList: LiveData<List<Crossword>> by lazy(LazyThreadSafetyMode.NONE) {
        loadCrosswords(levelCategory)
    }

    private fun loadCrosswords(levelCategory: LevelCategory): LiveData<List<Crossword>> {
        return repository.getCrosswords(levelCategory)
    }

}