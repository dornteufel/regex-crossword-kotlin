package de.chagemann.regexcrossword.features.game

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.switchMap
import de.chagemann.regexcrossword.db.Crossword
import de.chagemann.regexcrossword.db.CrosswordRepository
import de.chagemann.regexcrossword.db.LevelCategory

class GameActivityViewModel(private val app: Application, private val name: String) : AndroidViewModel(app) {

    private val repository: CrosswordRepository by lazy {
        CrosswordRepository(app)
    }

    val crossword: LiveData<Crossword> by lazy {
        loadCrossword(name)
    }

    val crosswordsForCurrentCategory: LiveData<List<Crossword>> = crossword.switchMap { crossword ->
        getCrosswords(crossword.levelCategory)
    }

    fun markLevelAsFinished() {
        repository.markLevelAsCompleted(name)
    }

    private fun loadCrossword(name: String): LiveData<Crossword> {
        return repository.getCrossword(name)
    }

    private fun getCrosswords(levelCategory: LevelCategory): LiveData<List<Crossword>> {
        return repository.getCrosswords(levelCategory)
    }
}