package de.chagemann.regexcrossword.features.selectlevel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.chagemann.regexcrossword.db.LevelCategory

class SelectLevelActivityViewModelFactory(private val application: Application, private val levelCategory: LevelCategory) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SelectLevelActivityViewModel(application, levelCategory) as T
    }

}