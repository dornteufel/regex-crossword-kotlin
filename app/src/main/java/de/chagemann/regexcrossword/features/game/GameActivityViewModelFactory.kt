package de.chagemann.regexcrossword.features.game

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class GameActivityViewModelFactory(private val application: Application, private val levelNameResKey: String) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GameActivityViewModel(application, levelNameResKey) as T
    }
}