package de.chagemann.regexcrossword.features.selectlevel

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.databinding.SelectLevelActivityBinding
import de.chagemann.regexcrossword.db.LevelCategory
import de.chagemann.regexcrossword.extensions.TAG
import de.chagemann.regexcrossword.features.game.GameActivity
import org.jetbrains.anko.toast

class SelectLevelActivity : AppCompatActivity() {

    private lateinit var binding: SelectLevelActivityBinding

    private lateinit var category: LevelCategory

    private lateinit var viewModel: SelectLevelActivityViewModel

    companion object {
        fun newIntent(context: Context, categoryName: LevelCategory): Intent {
            return Intent(context, SelectLevelActivity::class.java)
                .putExtra(context.getString(R.string.extra_key_category), categoryName)
        }

        const val START_LEVEL = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SelectLevelActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        (intent.getSerializableExtra(getString(R.string.extra_key_category)) as? LevelCategory)?.let {
            category = it
        }

        if (!::category.isInitialized) {
            Log.e(this.TAG, "Got null category from parent or child activity")
            finish()
            return
        }

        observeViewModel()
        afterViews()
    }

    private fun afterViews() = with(binding) {
        levelRecyclerView.layoutManager = LinearLayoutManager(this@SelectLevelActivity)
        levelRecyclerView.adapter = SelectLevelAdapter(this@SelectLevelActivity, listOf())
    }

    private fun observeViewModel() {
        viewModel = ViewModelProvider(
            this,
            SelectLevelActivityViewModelFactory(this.application, category)
        )[SelectLevelActivityViewModel::class.java]

        viewModel.crosswordList.observe(this, { crosswordList ->
            invalidateOptionsMenu()

            val rv = binding.levelRecyclerView
            rv.swapAdapter(SelectLevelAdapter(this, crosswordList), false)

            (rv.adapter as SelectLevelAdapter).clickListener.observe(this, { position ->

                val crossword = crosswordList[position]
                startActivityForResult(
                    GameActivity.newIntent(this, crossword.name),
                    START_LEVEL
                )
            })
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            START_LEVEL -> {
                (data?.getSerializableExtra(getString(R.string.extra_key_category)) as? LevelCategory)?.let {
                    this.category = it
                }
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.level_select, menu)
        if (viewModel.crosswordList.value?.all { crossword -> crossword.levelCompleted } == true) {
            menu.findItem(R.id.item_category_finished)?.isVisible = true
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.item_category_finished -> {
                toast(R.string.icon_category_finished_explanation)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
