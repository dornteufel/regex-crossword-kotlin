package de.chagemann.regexcrossword.db

data class CrosswordCategory(
        val levelCategory: LevelCategory,
        val levelCompleted: Int = 0,
        val levelTotal: Int
)
