package de.chagemann.regexcrossword.db

import android.content.Context
import android.util.Log
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.chagemann.regexcrossword.extensions.SingletonHolder
import de.chagemann.regexcrossword.extensions.TAG
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import java.io.InputStreamReader


@Database(entities = [Crossword::class], version = 4)
@TypeConverters(CrosswordTypeConverter::class)
abstract class CrosswordDatabase : RoomDatabase() {

    abstract fun crosswordDao(): CrosswordDao

    companion object : SingletonHolder<CrosswordDatabase, Context>({ context ->
        Room.databaseBuilder(context, CrosswordDatabase::class.java, "crossword_database")
                .addCallback(object : Callback() {
                    override fun onOpen(db: SupportSQLiteDatabase) {
                        super.onOpen(db)
                        doAsync {
                            try {
                                val assetManager = context.assets
                                val inputStream = assetManager.open("crosswords.json")
                                val gson = Gson()
                                val reader = InputStreamReader(inputStream)

                                val listType = object : TypeToken<List<Crossword>>() {}.type
                                val crosswordList = gson.fromJson<List<Crossword>>(reader, listType)

                                CrosswordDatabase.getInstance(context).crosswordDao().insertWithoutReplacing(*crosswordList.toTypedArray())
                                Log.w(TAG, "Initialized DB with ${crosswordList.size} crosswords")

                            } catch (t: Throwable) {
                                uiThread {
                                    context.longToast("Failed to build initial crosswords, please report the bug and reinstall the app!")
                                }
                                Log.e(TAG, "", t.cause)
                            }
                        }
                    }
                })
                .build()
    })
}

class CrosswordTypeConverter {

    companion object {
        @TypeConverter
        @JvmStatic
        fun fromStringList(value: List<String>): String {
            val gson = Gson()
            val type = object : TypeToken<List<String>>() {}.type
            return gson.toJson(value, type)
        }

        @TypeConverter
        @JvmStatic
        fun toStringList(value: String): List<String> {
            val gson = Gson()
            val type = object : TypeToken<List<String>>() {}.type
            return gson.fromJson(value, type)
        }

        @TypeConverter
        @JvmStatic
        fun fromLevelCategory(type: LevelCategory): Int = type.ordinal

        @TypeConverter
        @JvmStatic
        fun toLevelCategory(ordinal: Int): LevelCategory = LevelCategory.values().first { it.ordinal == ordinal }
    }

}