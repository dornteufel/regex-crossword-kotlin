package de.chagemann.regexcrossword.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CrosswordDao {

    @Query("SELECT * from crossword_table WHERE name LIKE :name")
    fun getCrossword(name: String): LiveData<Crossword>

    @Query("SELECT * from crossword_table ORDER BY levelCategory ASC")
    fun getCrosswords(): LiveData<List<Crossword>>

    @Query("SELECT * from crossword_table WHERE levelCategory LIKE :levelCategory ORDER BY name ASC")
    fun getCrosswords(levelCategory: LevelCategory): LiveData<List<Crossword>>

    @Query("SELECT levelCategory, SUM(levelCompleted) as levelCompleted, COUNT(*) as levelTotal from crossword_table GROUP BY levelCategory ORDER BY levelCategory ASC")
    fun getCrosswordCategories(): LiveData<List<CrosswordCategory>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg crosswords: Crossword)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWithoutReplacing(vararg crosswords: Crossword)

    @Query("UPDATE crossword_table SET levelCompleted = 1 WHERE name = :name")
    fun markLevelAsCompleted(name: String)

    @Query("DELETE FROM crossword_table")
    fun deleteAll()
}