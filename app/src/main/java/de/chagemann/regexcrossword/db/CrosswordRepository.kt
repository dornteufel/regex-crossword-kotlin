package de.chagemann.regexcrossword.db

import android.app.Application
import androidx.lifecycle.LiveData

class CrosswordRepository(private val application: Application) {

    private val crosswordDao: CrosswordDao by lazy {
        CrosswordDatabase.getInstance(application).crosswordDao()
    }

    fun getAllCrosswords(): LiveData<List<Crossword>> {
        return crosswordDao.getCrosswords()
    }

    fun getCrosswordCategories(): LiveData<List<CrosswordCategory>> {
        return crosswordDao.getCrosswordCategories()
    }

    fun getCrosswords(levelCategory: LevelCategory): LiveData<List<Crossword>> {
        return crosswordDao.getCrosswords(levelCategory)
    }

    fun getCrossword(name: String): LiveData<Crossword> {
        return crosswordDao.getCrossword(name)
    }

    fun insert(crossword: Crossword) {
        crosswordDao.insert(crossword)
    }

    fun deleteAll() {
        crosswordDao.deleteAll()
    }

    fun markLevelAsCompleted(name: String) {
        crosswordDao.markLevelAsCompleted(name)
    }
}